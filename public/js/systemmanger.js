    // 取得所有 tag名稱為script的 element
    var scripts = document.getElementsByTagName('script');
    // 取得最後一個 (也就是現在執行的這一個 )element
    var currentScript = scripts[scripts.length - 1];
    //已初始化

    var hasData = JSON.parse(currentScript.getAttribute('data'));
    console.log(hasData);
    // console.log(hasData."hasdata");
    console.log(hasData.url);
    // console.log(hasData)


    if (hasData.hasdata == 1) {

        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var data = { '_token': CSRF_TOKEN };
        var post_url = hasData.url + "/systemmanger/hasdata";
        var request = $.post({
            data: data,
            url: post_url
        });
        request.done(function(response) {
            var res = response.data;
            console.log(response);
            console.log(jQuery.type(response));
            console.log(response.Password);
            // console.log(JSON.parse(response));
            //step 1 
            $("#password").val(res.Password);
            $("#intKEY").val(res.KEY);
            $("#OpenIP").val(res.OpenIP);
            $("#PortNo").val(res.PortNo);
            $("#strQRCodePRDSite").val(res.QRCodePRDSite);
            if (res.InvitationFuncStatus == 1) {
                $("input[name='defaultInviteCode']").prop('checked', true);
            } else {
                $("input[name='defaultInviteCode']").prop('checked', false);
            }
            if (res.RevenueScale == 1) {
                $("input[name='brokerageCalPercent']").prop('checked', true);
            } else {
                $("input[name='brokerageCalPercent']").prop('checked', false);
            }
            //select 直属佣金结算时间
            $("#downLineweek").val(res.DistributorCalculatedDay).prop('selected', true);
            $("#DownLineTime").val(res.DistributorCalculatedTime);
            //select 代理佣金结算时间
            $("#Agentweek").val(res.AgentCalculatedDay).prop('selected', true);
            $("#AgentTime").val(res.AgentCalculatedTime);
            //提现提示内容
            $("#textarea1").val(res.WithdrawMessage);
            //每日可提现时间
            $("input[name='strDayWithdrawStartTime']").val(res.DayWithdrawStartTime);
            $("input[name='strDayWithdrawEndTime']").val(res.DayWithdrawEndTime);
            //每日可提現次數上限
            $("#withdrawnLimit").val(res.DayWithdrawLimitTimes);
            //單次提現上限
            $("#withdrawnLimitcash").val(res.WithdrawLimitPerTime);
            //每次提現間隔
            $("#withdrawninterval").val(res.WithdrawIntervalPerTime);
            //代理层级限制
            $("input[name='limitRule1']").val(res.LCondition1);
            $("input[name='limitRule2']").val(res.LCondition2);
            $("input[name='limitRule3']").val(res.LCondition3);
            //代理層級異常警示
            $("input[name='warningRule1']").val(res.ACondition1);
            $("input[name='warningRule2']").val(res.ACondition2);
            $("input[name='warningRule3']").val(res.ACondition3);
            $("input[name='warningRule4_1']").val(res.ACondition40);
            $("input[name='warningRule4_2']").val(res.ACondition41);

            //上傳檔案提示
            if (res.IsUnLimitAgentDTUpload == 1) {
                $("input[name='unlimit1']").attr('placeholder', '已上傳完無限代理表');
            }
            if (res.IsVipAndFriendDTUpload == 1) {
                $("input[name='vipAndFriendship1']").attr('placeholder', '已上傳完VIP等級與好友數量表');
            }

        });
        request.fail(function(xhr) {
            console.log(xhr);
        });

    }


    //[ReturnCode] => 200
    // [ReturnVal] => 成功!!
    // [Password] => 3529wa2s
    // [ConfigID] => 1
    // [KEY] => qp0DoxEH
    // [OpenIP] => 220.55.66.22
    // [PortNo] => 80
    // [QRCodePRDSite] => http://api.ukyo.idv.tw
    // [InvitationFuncStatus] => 0
    // [RevenueScale] => 0
    // [DistributorCalculatedDay] => 0
    // [DistributorCalculatedTime] => 00:00:00
    // [AgentCalculatedDay] => 0
    // [AgentCalculatedTime] => 00:00:00
    // [WithdrawMessage] => '提現時間每日早上8點~晚上11點'
    // [DayWithdrawStartTime] => 08
    // [DayWithdrawEndTime] => 23
    // [DayWithdrawLimitTimes] => 2
    // [WithdrawLimitPerTime] => 100000
    // [WithdrawIntervalPerTime] => 01:00
    // [IsUnLimitAgentDTUpload] => 0
    // [IsVipAndFriendDTUpload] => 0
    // [CollectDate] => 2019-05-06 13:44:52.680
    // [LCondition1] => 1
    // [LCondition2] => 2
    // [LCondition3] => 3
    // [ACondition1] => 999
    // [ACondition2] => 9
    // [ACondition3] => 19
    // [ACondition40] => 19
    // [ACondition41] => 9
