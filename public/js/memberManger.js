    // 取得所有 tag名称为script的 element
    var scripts = document.getElementsByTagName('script');
    // 取得最后一个 (也就是现在执行的这一个 )element
    var currentScript = scripts[scripts.length - 1];
    //已初始化

    var url = currentScript.getAttribute('url');
    var token = currentScript.getAttribute('token');

    $(".section").hide();

    $("#VendorConfrim").on('click', function() {
        console.log(token);
        $("input[name='_token']").val(token);
        $(".addVendor").submit();


    });
    //clipboard
    var clipboard = new ClipboardJS('.clip');

    clipboard.on('success', function(e) {
        // console.info('Action:', e.action);
        // console.info('Text:', e.text);
        // console.info('Trigger:', e.trigger);

        e.clearSelection();
    });

    clipboard.on('error', function(e) {
        // console.error('Action:', e.action);
        // console.error('Trigger:', e.trigger);
    });

    $("#point").on('keyup ', function() {
        $(".section").show();
        var coin = $("#point").val();
        var view = "<img src='../../images/logo/login-logo.png' alt='Materialize'>" + coin + " 人民币 兑换 " + (parseInt(coin) * 2.5) + " 点数 </p>";
        $(".displayChangeCoin").html(view);
    });

    $("#confrim").on('click', function() {
        if ($(".loginstatus").prop('checked') == true) {
            $(".loginstatus").val(1);
        }
        $("#createorModify").submit();
    });

    //addUser And Create signtrue
    $(".addUser").on('click', function() {
        $('input').val("");
        $(".money").show();
        $("#filled-in-box").prop('checked', false);
        $(".modal-content").find('h4').text('新增帐号');
        $("#createorModify").find("input[name='_token']").val(token);
        $("input[name='status']").prop('checked', true);
        // $("#createorModify").attr('action', url + '/memberManger/addMember');

        //create signatrue

    });

    $(".addVendor").on('click', function() {
        $(".vendortitle").html('新增代理商');
        $("input[name='_token']").val(token);

    })

    // 
    // (弹跳视窗) edit 修改 
    function modify(view) {
        $(".money").hide();
        $("label").attr('class', 'active');
        $(".modal-content").find('h4').text('修改帐号');
        $("#createorModify").attr('action', url + '/memberManger/modifyMmember/' + view.id);
        //給password一個預設值
        $("#upw").val(123456);
        $("#companyname").val(view.company);
        $("#user").val(view.user).prop('readonly', true);
        $("#nickname").val(view.username);
        $("select[name='vid']").val(view.vid).prop('selected', true);
        $("select[name='vid']").material_select();
        $("select[name='pid']").val(view.pid).prop('selected', true);
        $("select[name='pid']").material_select();
        $(".loginstatus").prop('checked', view.can_login);
        $("input[name='_method']").val('PUT');
        $(".super").hide();
        // if (view.is_admin == 1) {
        //     $("input[name='is_admin']").prop('checked', true);
        // } else {
        //     $("input[name='is_admin']").prop('checked', false);
        // }
    }




    function addMoney(view) {
        $(".addPoint").attr('action', url + '/memberManger/addPoint/' + view.cid + "/" + view.id);
        console.log(view);
        var display = "現有點數:" + view.point + "&nbsp; 點";
        $(".flight-card-date").html(display);

        $("#point2").on('keyup', function() {
            var $value = $(this).val();

            $("#addyen").html($value + "元");
            var addpoint = (parseInt($value) * 2.5) + "點";
            $("#addpoint").html(addpoint);
        });

        $("#addMoney").on('click', function() {
            $(".addPoint").submit();
        });

    }

    function modifyVendor(view) {
        console.log(view);
        $("label").attr('class', 'active');
        $("#modal2 >.modal-content").find('h4').text('修改代理商');
        $(".addVendor").attr('action', url + '/memberManger/modifyVendor/' + view.id);
        $("#vendorMethod").val('PUT');
        $("#vendorKind").val(view.kind).prop('selected', true);
        $("#vendorKind").material_select();
        $("#vendor").val(view.user);
        $("#vendorpsw").val(view.password);
        $("#remark").val(view.remark);

    }


    function deleteVendor(view) {


        $.ajax({
            url: '/memberManger/deleteVendor/' + view.id,
            type: 'delete',
            headers: {
                'X-CSRF-TOKEN': token,
            },
            success: function(response) {
                console.log(response);

                // swal({title:});
                // 
                if (response.errorCode == 200) {
                    swal({
                        title: '成功',
                        type: 'success',
                        text: response.msg,
                    });
                } else {
                    swal({
                        title: '失敗',
                        type: 'warning',
                        text: response.msg,
                    });
                }
            },
        });
        setTimeout(function() { location.reload(); }, 2000);

    }

    function touchMoney(id) {
        $("#money_" + id).hide();
        $("#preload_" + id).show();

        $.ajax({
            url: '/memberManger/getVendorBalance/' + id,
            type: 'get',
            headers: {
                'X-CSRF-TOKEN': token,
            },
            success: function(response) {

                $("#preload_" + id).hide();

                // swal({title:});
                // 
                if (response.status == 200) {
                    $("#show_" + id).text(response.result);

                } else {
                    $("#show_" + id).text(response.msg);

                }
                $("#money_" + id).show();

            },
        });
    }



    $(function() {
        $(".preloader-wrapper").hide();
        $("#createorModify").validate({
            rules: {
                user: {
                    required: true,
                    minlength: 5
                },

                curl: {
                    required: true,
                    url: true
                },
                crole: "required",
                ccomment: {
                    required: true,
                    minlength: 15
                },
                cgender: "required",
                cagree: "required",
            },
            //For custom messages
            messages: {
                uname: {
                    required: "Enter a username",
                    minlength: "Enter at least 5 characters"
                },
                curl: "Enter your website",
            },
            errorElement: 'div',
            errorPlacement: function(error, element) {
                var placement = $(element).data('error');
                if (placement) {
                    $(placement).append(error)
                } else {
                    error.insertAfter(element);
                }
            }
        });




        $("#createVendor").validate({
            rules: {
                vendor: {
                    required: true,
                    minlength: 4
                },

                vendorpsw: {
                    required: true,
                    minlength: 4
                },

            },
            //For custom messages
            messages: {
                uname: {
                    required: "Enter a username",
                    minlength: "Enter at least 5 characters"
                },
                curl: "Enter your website",
            },
            errorElement: 'div',
            errorPlacement: function(error, element) {
                var placement = $(element).data('error');
                if (placement) {
                    $(placement).append(error)
                } else {
                    error.insertAfter(element);
                }
            }
        });
        //test switch on / off


    });

    //custom js
