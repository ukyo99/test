<?php

namespace App\Http\Controllers;

use Alert;
use App\Models\flag;
use App\Models\rate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Session;

class IndexController extends Controller {

    public function index(Request $request) {
        //define
        if (empty(Session::get('sflag'))) {
            $sflags = flag::where('status', 0)->get();
            Session::put('sflag', $sflags);
        } else {
            $sflags = Session::get('sflag');
        }
        if (empty(Session::get('rates'))) {

            //已經有的匯率
            $rates = rate::all();
            Session::put('rates', $rates);
            //可以新增的匯率

        } else {

            $rates = Session::get('rates');
        }
        return view('home', ['data' => $rates, 'selectFlags' => $sflags]);

    }

    public function search(Request $request) {

        $flag = flag::where('name', 'like', '%' . trim(strtoupper($request->q)) . '%')->orWhere('coinname', strtoupper($request->q))->first();

        if (empty($flag->id) || empty($request->q)) {

            Alert::warning('未搜尋到資料', '警告');
            return redirect()->Route('Home')->withInput();
        }

        $rates = rate::where('fid', $flag->id)->get();

        return view('home', ['data' => $rates, 'selectFlags' => Session::get('sflag')]);

    }

    public function create(Request $request) {

        $flag = flag::find($request->sflags);

        $insertRate = rate::create([
            'cashbuy'  => $request->cashbuy ?: 0,
            'cashsell' => $request->cashsell ?: 0,
            'spotbuy'  => $request->spotbuy ?: 0,
            'spotsell' => $request->spotsell ?: 0,
            'fid'      => $flag->id,
        ]);

        $flag->status = 1;

        if ($insertRate->id) {
            $rates = rate::all();
            Session::put('rates', $rates);
            $flag->save();
            $sflags = flag::where('status', 0)->get();
            Session::put('sflag', $sflags);

            Alert::success('新增成功', '成功');

        } else {
            Alert::warning('新增失敗', '警告');

        }

        return redirect()->Route('Home');

    }
    public function delete(Request $request) {

        $rate = rate::find($request->id);
        $flag = flag::find($rate->fid);
        $rate->delete();
        $rates = rate::all();
        Session::put('rates', $rates);
        $flag->status = 0;
        $flag->save();
        if ($flag->id) {
            $sflags = flag::where('status', 0)->get();
            Session::put('sflag', $sflags);
            return Response()->json(['status' => 200, 'msg' => '刪除成功']);
        } else {

            return Response()->json(['status' => 403, 'msg' => '刪除失敗']);

        }
    }

    public function update(Request $request) {

        $rate           = rate::find($request->id);
        $rate->cashbuy  = $request->cashbuy;
        $rate->cashsell = $request->cashsell;
        $rate->spotbuy  = $request->spotbuy;
        $rate->spotsell = $request->spotsell;
        $rate->save();
        $rate = rate::all();
        Session::put('rates', $rate);
        Session::put('sflag', flag::all());
        return Response()->json(['status' => 200, 'msg' => '修改成功']);

    }
}
