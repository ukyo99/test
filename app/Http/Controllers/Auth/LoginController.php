<?php

namespace App\Http\Controllers\Auth;

use Alert;
use App\Http\Controllers\Controller;
use Captcha;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Session;

class LoginController extends Controller {
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
     */
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo   = '/';
    protected $password_key = '';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest')->except('logout');
    }

    public function loginView() {

        return view('/auth/login', ['captcha_src' => Captcha::src()]);
    }

    public function changeCaptcha() {
        return response()->json(['captcha_src' => Captcha::src()]);
    }

    public function authCheck(request $request) {

        //required表示必填 captcha表示进行验证码验证
        $rules = [
            'captcha' => 'required|captcha',
        ];
        // 自定义消息
        $messages = [
            'captcha.required' => '请输入验证码',
            'captcha.captcha'  => '请输入正确验证码',
        ];

        //对验证码字段进行验证
        $validator = \Validator::make($request->all(), $rules, $messages);
        if ($validator->passes()) {
            //验证通过

            if (Auth::attempt(['user' => $request->input('user'), 'password' => $request->input('password')])) {

                if ($request->input('rememberme') == 'on') {
                    $user = Auth::user();
                    Auth::login($user, true);
                }

                Alert::success('你已顺利登入', '成功');
                return redirect()->route('Home');

            } else {
                Alert::error('帐号密码输入错误', '错误');
                return redirect()->route('Login')->withInput();

            }
        } else {

            Alert::error('验证码输入错误', '错误');
            return redirect()->route('Login')->withInput();

        }
        return redirect()->route('Home');

    }

    public function logout(request $request) {

        //登出;
        Auth::logout();

        //清空整個session;
        Session::flush();

        return redirect()->route('Login');

    }
}
