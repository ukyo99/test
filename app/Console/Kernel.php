<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        // \App\Console\Commands\Commission::class,
        // \App\Console\Commands\TestLog::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $logname = date('Y-m-d') . 'ScheduleExec.log';
        $log_file_path = storage_path("/logs/" . $logname);

        $stringExe = 'curl ' . env('APP_CRON_URL') . "/api/NiuBaiFa/receive";
        $schedule->exec($stringExe)->everyFiveMinutes();
        // $stringExe1 = 'curl ' . env('APP_CRON_URL') . "/Makeup";

        // //抓取批次號重算兩次
        // $schedule->exec($stringExe1)->twiceDaily(5, 8);

        // switch ($execKind) {
        //     case 0:
        //         // $debug = $schedule->exec($stringExe)->everyMinute();
        //         //
        //         $schedule->exec($stringExe)->dailyAt('04:00');

        //         break;
        //     case 1:
        //         $schedule->exec($stringExe)->tuesdays()->at('4:00');

        //         break;
        //     case 2:
        //         $schedule->exec($stringExe)->wednesdays()->at('4:00');

        //         break;
        //     case 3:
        //         $schedule->exec($stringExe)->thursdays()->at('4:00');

        //         break;
        //     case 4:
        //         $schedule->exec($stringExe)->fridays()->at('4:00');

        //         break;
        //     case 5:
        //         $schedule->exec($stringExe)->saturdays()->at('4:00');

        //         break;
        //     case 6:
        //         $schedule->exec($stringExe)->sundays()->at('4:00');

        //         break;
        //     case 7:
        //         $schedule->exec($stringExe)->mondays()->at('4:00');

        //         break;

        // }
        $log_info = ['date' => date('Y-m-d H:i:s'), 'call' => $stringExe];
        // 記錄 JSON 字串
        $log_info_json = json_encode($log_info) . PHP_EOL;
        // File::append($log_file_path, $log_info_json);
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
