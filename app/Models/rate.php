<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class rate extends Model {
    //
    //

    protected $table      = 'rate';
    protected $primaryKey = 'id';
    protected $guarded    = [];

    public function flags() {

        return $this->belongsTo('App\Models\flag', 'fid', 'id');
    }
}
