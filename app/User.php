<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable {

    protected $connection = 'mysql';
    // protected $append     = ['custom'];

    use Notifiable, HasApiTokens;
    // $findUser = DB::select("SELECT * FROM dbo.users WHERE username = '{$request->input('user')}'");
    // print_r($findUser);

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'member';
    // public $timestamps    = false;
    // protected $primaryKey = 'MemberID';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'MemberPassword',
    ];

    public function findForPassport($username) {
        return $this->where('username', $username)->first();
    }

    /**
     * Overrides the method to ignore the remember token.
     */
    public function setAttribute($key, $value) {
        $isRememberTokenAttribute = $key == $this->getRememberTokenName();
        if (!$isRememberTokenAttribute) {
            parent::setAttribute($key, $value);
        }
    }
}
