<?php
namespace App\Tools;

use Illuminate\Database\Eloquent\Model;

class Conversion extends Model
{
    //ObjectToArray
    public static function OTA($params)
    {
        return json_decode(json_encode($params), true);
    }
}
