<?php

namespace App\Tools;

use App\Models\cashlog;
use App\Models\member;
use App\Models\messageRecord;
use Illuminate\Database\Eloquent\Model;

class validate extends Model
{

    public function baseValidate($params)
    {
        try {
            if (!$params['user']) {

                throw new \Exception('Parameter not filled or wrong', 404);
            }

            if (!$params['title']) {
                throw new \Exception('title Parameter not filled or wrong', 404);

            }
            if (!$params['message']) {
                throw new \Exception('title Parameter not filled or wrong', 404);

            }

            if (!$params['cellphone']) {
                throw new \Exception('title Parameter not filled or wrong', 404);

            }

            $check = $this->CheckSum($params);

            if ($check['status'] != 200) {
                throw new \Exception('checksum未通過', 404);

            }

            return ['status' => 200];

        } catch (\Exception $e) {

            return [
                'status' => $e->getCode(),
                'msg'    => $e->getMessage(),
            ];
        }
    }

    /**
     * [CheckSum 檢查碼順便檢查有無此用戶]
     * @param [type] $params [description]
     */
    public function CheckSum($params)
    {
        $member = member::where('user', $params['user'])->first();

        if ($member) {
            $member         = $member->toArray();
            $token['token'] = $member['apitoken'];
            $checksum       = $params['checksum'];
            unset($params['checksum']);
            $check = array_merge($token, $params);
            $sum   = "";
            foreach ($check as $key => $value) {
                $sum .= $value;

            }
            if ($checksum == md5($sum)) {

                return ['status' => 200];
            } else {
                return ['status' => 400];
            }

        } else {
            return ['status' => 400];
        }

    }

    public function CheckSumOf367($params)
    {
        $salt     = ["ApiLoveToPlay"];
        $member   = member::where('user', $params['User'])->first();
        $checksum = $params['CheckSum'];
        unset($params['CheckSum']);

        if ($member) {

            $check = array_merge($salt, $params);

            $sum = "";

            foreach ($check as $key => $value) {
                $sum .= $value;
            }

            if ($checksum == md5($sum)) {

                return ['status' => 200];
            } else {

                // echo "<pre>";
                // print_r($checksum);

                // print_r(md5($sum));
                // exit;

                return ['status' => 400];
            }

        } else {

            return ['status' => 404, 'msg' => 'No such User'];
        }

    }

    public function messagePer($params)
    {
        return ceil(mb_strlen($params, 'utf-8') / 67) >= 1 ? ceil(mb_strlen($params, 'utf-8') / 67) : 1;
    }

    public function wirteOneRecord($params)
    {

        $max = count($params['response']['vendor_order']);

        $splitCellPhone = explode(",", $params['process']['cellphone']);
        foreach ($splitCellPhone as $key => $value) {

            $record = messageRecord::create([
                'mid'            => $params['member']['id'],
                'vendor_order'   => $params['response']['vendor_order'][0],
                'Ymd'            => date('Ymd'),
                'requestTime'    => $params['cash']['updated_at'],
                'cellphone'      => $value,
                'message_per'    => $params['response']['msg_per'],
                'status'         => $params['response']['status'],
                'responseTime'   => date('Y-m-d H:i:s'),
                'log'            => $params['response']['log'],
                'reciver_status' => 0,
                'message'        => trim("【" . $params['process']['title'] . "】" . $params['process']['message']),
            ]);

            if (end($splitCellPhone) == $value) {
                $msgrecords .= $record->id;
            } else {
                $msgrecords .= $record->id . ",";
            }

        }
        $cashlog               = cashlog::find($params['insert']['cashlog']);
        $cashlog->msgrecord_id = $msgrecords;
        $cashlog->save();

        return $record->id;

    }
    public function wirteOneRecordNiuBaiFa($params)
    {

        $max = count($params['response']['vendor_order']);

        $splitCellPhone = explode(",", $params['process']['cellphone']);
        foreach ($splitCellPhone as $key => $value) {

            $record = messageRecord::create([
                'mid'            => $params['member']['id'],
                'vendor_order'   => $params['response']['vendor_order'],
                'Ymd'            => date('Ymd'),
                'requestTime'    => $params['cash']['updated_at'],
                'cellphone'      => $value,
                'message_per'    => $params['response']['msg_per'],
                'status'         => $params['response']['status'],
                'responseTime'   => date('Y-m-d H:i:s'),
                'log'            => $params['response']['log'],
                'reciver_status' => 0,
                'message'        => $params['process']['message'],
            ]);

            if (end($splitCellPhone) == $value) {
                $msgrecords .= $record->id;
            } else {
                $msgrecords .= $record->id . ",";
            }

        }

        $cashlog               = cashlog::find($params['insert']['cashlog']);
        $cashlog->msgrecord_id = $msgrecords;
        $cashlog->save();

        return $msgrecords;

    }
}
