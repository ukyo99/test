<?php
namespace App\Tools;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Session;

class pageCreage extends Model
{

    public $perPage;

    public function __construct()
    {
        $this->perPage = 15;

    }
    //$page->page(['page' => $request->input('page'), 'Session' => 'vendorManger', 'perPage' => 15])

    /**
     * [page 簡單用slice創造頁麵]
     * @param  [type] $params['page'] [description]
     * @param  [type] $params ['Session'][description]
     * @param  [type] $params['perPage'] [description]
     * @param  [type] $params['query'] [description]
     *
     * @return [type]         [description]
     */
    public function page($params)
    {

        $page = $params['page'] ?: 1;

        $this->perPage = $params['perPage'] ? $params['perPage'] : $this->perPage;

        $data = Session::get($params['Session']);

        $currentPage = LengthAwarePaginator::resolveCurrentPage('page', $page);

        $dataCollection = collect($data);
        $currentPageItems = $dataCollection->slice(($currentPage * $this->perPage) - $this->perPage, $this->perPage)->all();

        if (isset($params['query'])) {

            $paginatedItems = new LengthAwarePaginator($currentPageItems, count($dataCollection), $this->perPage, $page, ['query' => $params['query']]);
        } else {
            $paginatedItems = new LengthAwarePaginator($currentPageItems, count($dataCollection), $this->perPage, $page);
        }

        if (empty($params['setPath'])) {
            $paginatedItems->setPath("");
        } else {
            $paginatedItems->setPath($params['query']);

        }

        return $paginatedItems;

    }
}
