<?php

namespace App\Providers;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {
    /**
     * menu is use twig index  navbar
     *
     *
     */

    protected $menu = [
        ["id" => 0, "name" => "平台佣金分析", "href" => "/brokerage", "icon" => "assignment_ind"],
        ["id" => 1, "name" => "代理关联分析", "href" => "/agentrelation", "icon" => "accessibility"],
        ["id" => 2, "name" => "系统管理", "href" => "/systemmanger", "icon" => "settings_system_daydream"],
        ["id" => 3, "name" => "佣金纪录", "href" => "/brokeragerecord", "icon" => "monetization_on"],
        ["id" => 4, "name" => "提现纪录", "href" => "/withdrawrecord", "icon" => "money_off"],
        ["id" => 5, "name" => "会员管理", "href" => "/membermanger", "icon" => "people_outline"],
        ["id" => 6, "name" => "异常警讯纪录", "href" => "/alert", "icon" => "alarm"],
        ["id" => 7, "name" => "用户操作纪录", "href" => "/operating", "icon" => "event_note"],
        ["id" => 8, "name" => "登出", "href" => "/logout", "icon" => "power_settings_new"],

    ];

    public $menu_billborad = [];

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        // $this->menu_billborad = DB::select("SET NOCOUNT ON;EXEC dbo.USP_GET_AgentMembersCntAndExecTimeInfo");

        View::share('nav', $this->menu);

        // View::share('billborad', $this->menu_billborad[0]);

        Paginator::defaultView('pagination::laravel_materialize_pagination');
        Paginator::defaultSimpleView('pagination::laravel_materialize_pagination');
        $user = auth::user();

        if ($user);
        View::share('user', $user);

        $this->app->resolving(LengthAwarePaginator::class, function ($paginator) {
            return $paginator->appends(array_except(Input::query(), $paginator->getPageName()));
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        $this->app->make('App\Tools\IdGenerator');
    }
}
