


create api (創建帳號)

url:http://367.plskyayg.com/api/Server/CreateMember
method :POST

params:
		account (帳號)
		password(密碼)
		checksum(檢查碼)

encrypt(加密所需參數):
		Salt:      AiplayDs2020     //塩質
		account (帳號)				//自定義
		password(密碼)				//自定義
encrypted checksum:
		md5(account+Salt+password)


response:
		status :200
		msg:新增成功
		
		------------
		status:402
		msg:member already exists



----------------------------------------------------
Login game (登入遊戲)

url:http://367.plskyayg.com/api/Server/LoginGame
method :POST

params:
		account (帳號)
		gameid(遊戲gameid) 補魚機:1001,1002
		checksum(檢查碼)

encrypt(加密所需參數):
		Salt:      AiplayDs2020     //塩質
		account (帳號)				//自定義
		gameid(遊戲gameid)	補魚機:1001,1002			//自定義
encrypted checksum:
		md5(gameid+Salt+account)


response:
		status :200
		msg:新增成功
		url:網址
		----------



----------------------------------------------------------
Transfer (轉帳)

url:http://367.plskyayg.com/api/Server/Transfer
method :POST

params:
		account (帳號)
		amount(金額) 可至小數點第二位
		oper_type(1存,0提)
		checksum(檢查碼)
		UserID
		gameKind 1001,1002
encrypt(加密所需參數):
		Salt:      AiplayDs2020     //塩質
		serial(交易單號)
		account (帳號)
		amount(金額)  
		oper_type(1存,0提)



encrypted checksum:
		md5(account+amount+Salt+oper_type)


response:
		status :200
		msg:新增成功
		time:回傳時間
		trans_id:對方交易id
		serial:我方交易單號
		balance:累計金額
		------------
		
		status :403
		msg:failed
		time:回傳時間
		serial:我方交易單號
		balance:累計金額

----------------------------------------------------------
getBalance (取餘額)

url:http://367.plskyayg.com/api/Server/getBalance
method :POST

params:
		account (帳號)

		checksum(檢查碼)

encrypt(加密所需參數):
		Salt:      AiplayDs2020     //塩質
		account (帳號)
encrypted checksum:
		md5(account+Salt)


response:
		status :200
		msg:新增成功
		serial:我方交易單號
		balance:當下金額
		
		------------
	
----------------------------------------------------
logout