<?php

namespace Tests\Unit;

// use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Models\rate;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class RateTest extends TestCase {
    /**
     * A basic unit test example.
     *
     * @return void
     */
    use WithoutMiddleware;
    // use DatabaseMigrations;

    // public function setUp(): void{
    //     parent::setUp();
    //     $this->artisan('db:seed');

    // }

    public function testDatabase() {
        $this->assertDatabaseHas('rate', [
            'fid' => 1,
        ]);

        $this->assertDatabaseHas('flag', [
            'name' => '美金',
        ]);
    }

    public function testMethod() {

        $this->call('GET', '/');
        $this->assertTrue(true);
    }
    /**
     * 新增多筆
     */
    // public function testcreateRate() {
    //     factory(rate::class, 5)->create();
    //     $rates = rate::all();

    //     $this->assertCount(count($rates), $rates);
    // }

    /**
     * 新增一筆
     * @return [type] [description]
     */
    public function testCreateOneRate() {
        $response = $this->call('POST', '/create', [
            'cashbuy'  => 20.2,
            'cashsell' => 20.2,
            'spotbuy'  => 20.2,
            'spotsell' => 20.2,
            'sflags'   => 17,
        ]);
        $rates = rate::all();

        $this->assertCount(count($rates), $rates);
    }

    public function testSearch() {

        $response = $this->call('GET', 'search', ['q' => 'USD']);

        $response->assertStatus(200);
    }

    public function testDelete() {

        $response = $this->json('DELETE', 'delete', ['id' => 9]);
        // $response->dump();
        $this->assertEquals(200, $response->status());
    }

    public function testUpdate() {

        $response = $this->json('PUT', 'update', [
            'id'       => 2,
            'cashbuy'  => 20,
            'cashsell' => 20,
            'spotbuy'  => 20,
            'spotsell' => 20,
        ]);
        $this->assertEquals(200, $response->status());

    }
}
