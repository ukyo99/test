<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRateTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('rate', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('cashbuy', 10, 5)->default(0);
            $table->decimal('cashsell', 10, 5)->default(0);
            $table->decimal('spotbuy', 10, 5)->default(0);
            $table->decimal('spotsell', 10, 5)->default(0);
            $table->integer('fid');
            $table->unique('fid');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('rate');
    }
}
