<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        DB::connection('mysql')->table('member')->insert([
            'user'       => 'admin',
            'password'   => Hash::make('microprogram'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        //flag 已使用

        DB::connection('mysql')->table('flag')->insert([
            'name'       => '美金',
            'coinname'   => 'USD',
            'path'       => 'US.png',
            'status'     => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),

        ]);
        DB::connection('mysql')->table('flag')->insert([
            'name'       => '港幣',
            'coinname'   => 'HKD',
            'path'       => 'hongkong.png',
            'status'     => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::connection('mysql')->table('flag')->insert([
            'name'       => '英磅',
            'coinname'   => 'GBP',
            'path'       => 'kingdom.png',
            'status'     => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::connection('mysql')->table('flag')->insert([
            'name'       => '澳幣',
            'coinname'   => 'AUD',
            'path'       => 'australia.png',
            'status'     => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::connection('mysql')->table('flag')->insert([
            'name'       => '加拿大幣',
            'coinname'   => 'CAD',
            'path'       => 'canada.png',
            'status'     => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::connection('mysql')->table('flag')->insert([
            'name'       => '新加坡幣',
            'coinname'   => 'SGD',
            'path'       => 'singapore.png',
            'status'     => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::connection('mysql')->table('flag')->insert([
            'name'       => '瑞士法郎',
            'coinname'   => 'CHF',
            'path'       => 'switzerland.png',
            'status'     => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::connection('mysql')->table('flag')->insert([
            'name'       => '日圓',
            'coinname'   => 'JPY',
            'path'       => 'japan.png',
            'status'     => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        ///對應flag

        DB::connection('mysql')->table('rate')->insert([
            'cashbuy'    => 30.425,
            'cashsell'   => 31.115,
            'spotbuy'    => 30.795,
            'spotsell'   => 30.895,
            'fid'        => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::connection('mysql')->table('rate')->insert([
            'cashbuy'    => 3.765,
            'cashsell'   => 3.981,
            'spotbuy'    => 3.901,
            'spotsell'   => 3.961,
            'fid'        => 2,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::connection('mysql')->table('rate')->insert([
            'cashbuy'    => 38.86,
            'cashsell'   => 40.98,
            'spotbuy'    => 39.86,
            'spotsell'   => 40.28,
            'fid'        => 3,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::connection('mysql')->table('rate')->insert([
            'cashbuy'    => 21.59,
            'cashsell'   => 22.37,
            'spotbuy'    => 21.86,
            'spotsell'   => 22.09,
            'fid'        => 4,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::connection('mysql')->table('rate')->insert([
            'cashbuy'    => 22.59,
            'cashsell'   => 23.5,
            'spotbuy'    => 22.98,
            'spotsell'   => 23.09,
            'fid'        => 5,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::connection('mysql')->table('rate')->insert([
            'cashbuy'    => 22.18,
            'cashsell'   => 23.09,
            'spotbuy'    => 22.67,
            'spotsell'   => 22.85,
            'fid'        => 6,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::connection('mysql')->table('rate')->insert([
            'cashbuy'    => 29.54,
            'cashsell'   => 30.74,
            'spotbuy'    => 30.2,
            'spotsell'   => 30.49,
            'fid'        => 7,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::connection('mysql')->table('rate')->insert([
            'cashbuy'    => 0.2665,
            'cashsell'   => 0.2793,
            'spotbuy'    => 0.2738,
            'spotsell'   => 0.2778,
            'fid'        => 8,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        //加一些未用到的flag

        DB::connection('mysql')->table('flag')->insert([
            'name'     => '南非幣',
            'coinname' => 'ZAR',
            'path'     => 'south.png',
            'status'   => 0,
        ]);
        DB::connection('mysql')->table('flag')->insert([
            'name'     => '瑞典幣',
            'coinname' => 'SEK',
            'path'     => 'sweden.png',
            'status'   => 0,
        ]);
        DB::connection('mysql')->table('flag')->insert([
            'name'     => '紐元',
            'coinname' => 'NZD',
            'path'     => 'newzealand.png',
            'status'   => 0,
        ]);
        DB::connection('mysql')->table('flag')->insert([
            'name'     => '泰幣',
            'coinname' => 'THB',
            'path'     => 'thailand.png',
            'status'   => 0,
        ]);
        DB::connection('mysql')->table('flag')->insert([
            'name'     => '菲國比索',
            'coinname' => 'PHP',
            'path'     => 'philippines.png',
            'status'   => 0,
        ]);
        DB::connection('mysql')->table('flag')->insert([
            'name'     => '印尼幣',
            'coinname' => 'IDR',
            'path'     => 'indonesia.png',
            'status'   => 0,
        ]);
        DB::connection('mysql')->table('flag')->insert([
            'name'     => '歐元',
            'coinname' => 'EUR',
            'path'     => 'eupra.png',
            'status'   => 0,
        ]);
        DB::connection('mysql')->table('flag')->insert([
            'name'     => '韓元',
            'coinname' => 'KRW',
            'path'     => 'korea.png',
            'status'   => 0,
        ]);
        DB::connection('mysql')->table('flag')->insert([
            'name'     => '越南盾',
            'coinname' => 'VND',
            'path'     => 'vietnam.png',
            'status'   => 0,
        ]);
        DB::connection('mysql')->table('flag')->insert([
            'name'     => '馬來幣',
            'coinname' => 'MYR',
            'path'     => 'malaysia.png',
            'status'   => 0,
        ]);
        DB::connection('mysql')->table('flag')->insert([
            'name'     => '人民幣',
            'coinname' => 'CNY',
            'path'     => 'china.png',
            'status'   => 0,
        ]);

    }
}
