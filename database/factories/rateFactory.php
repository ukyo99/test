<?php

use Faker\Generator as Faker;

$factory->define(App\Models\rate::class, function (Faker $faker) {
    return [
        'cashbuy'  => 20.2,
        'cashsell' => 20.2,
        'spotbuy'  => 20.2,
        'spotsell' => 20.2,
        'fid'      => random_int(10, 100),
    ];
});
