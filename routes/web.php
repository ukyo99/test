<?php

//登入
Route::get('/login', 'Auth\LoginController@loginView')->name('Login');

//驗證登入
Route::post('/login/authCheck', 'Auth\LoginController@authCheck')->name('authCheck');

//登出
Route::get('logout', 'Auth\LoginController@logout')->name('Logout');

//首頁
Route::get('/', 'IndexController@index')->name('Home')->middleware('auth');

//搜尋
Route::get('search', 'IndexController@search')->name('Search')->middleware('auth');

//新增
Route::post('create', 'IndexController@create')->name('Create')->middleware('auth');

//刪除
Route::delete('delete', 'IndexController@delete')->name('Delete')->middleware('auth');

//修改
Route::put('update', 'IndexController@update')->name('Update')->middleware('auth');
